**Linux Kernel Module for the Silicon Labs Si5346 digital PLL synthesiser**

Devicetree binding (ex. if device is at chip address 0x6F):

    si5346@6f {
        compatible = "linux,si5346";
        reg = <0x6f>;     
    };
    
On loading the module, the Si5346 device is probed, and initially programmed to 
generate a 88.0525 MHz clock on OUT0.

The clock signal on OUT0 can be re-programmed from userspace using the sysfs interface:
    
```
$ sudo -s
# echo 25 > /sys/bus/i2c/drivers/si5346/2-006f/si5346 
si5346 2-006f: Configuring output clock to 25 MHz
```

**NOTE:** Currently only 25 MHz, 88.0525 MHz, and 100 MHz are supported.

The current frequency of OUT0 can also be read back from the sysfs interface:

```
# cat /sys/bus/i2c/drivers/si5346/2-006f/si5346 
si5346 2-006f: Currently configured to output a 25 MHz clock
```   


    
