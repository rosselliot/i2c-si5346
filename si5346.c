/*
 * I2C master interface for programming Si5346 synthesizer
 *
 */

#include <linux/spinlock.h>
#include <linux/module.h>
#include <linux/sysfs.h>
#include <linux/slab.h>
#include <linux/init.h>
#include <linux/i2c.h>
#include <linux/of.h>
#include <linux/delay.h>
#include <linux/device.h>
/* Register configurations for {25 / 88.0525 / 100} MHz */
#include <si5346_regs_25.h>
#include <si5346_regs_88.h>
#include <si5346_regs_100.h>
#include <si5346_regs_O0_88_O2_100_O3_100.h>
#include <si5346_regs_O0_88_O2_150_allfreerunning.h>
#include <si5346_regs_O0_88_O2_150_O3_150.h>

#define DRIVER_NAME "si5346"
#define DEVICE_NAME "si5346"

#define PREAMBLE_WAIT 300000
#define ADDR_MASK 0x00FF

struct si5346_driver_data {
    struct i2c_client *client;
    struct bin_attribute bin;
    spinlock_t status_mutex;
    u8 status;
    char *cur_output;
    si5346_revd_register_t const* regs;
};

static int si5346_init(struct i2c_client *client, char *config)
{
    int i, ret, numReg;
    unsigned char curPage = 0, page, addr, data;
    char *strMHz = "";
    si5346_revd_register_t const* si5346_regs = si5346_revd_registers_88;
    struct si5346_driver_data *si5346_data = i2c_get_clientdata(client);


    /* If si5346 has not yet been configured, set default */
    if ( ! strcmp(si5346_data->cur_output, "")){
        strMHz = "88.0525";
        numReg = SI5346_REVD_REG_CONFIG_NUM_REGS_88;
    }
    /* Select register set based on config */
    /* Sysfs input strings terminate with newlines.
       sysfs_streq will compensate for newlines when
       comparing against values without newlines     */
    if (sysfs_streq(config, "25")) {
        si5346_regs = si5346_revd_registers_25;
        strMHz = "25";
        numReg = SI5346_REVD_REG_CONFIG_NUM_REGS_88;
    } else if (sysfs_streq(config, "88")) {
        si5346_regs = si5346_revd_registers_88;
        strMHz = "88.0525";
        numReg = SI5346_REVD_REG_CONFIG_NUM_REGS_25;
    } else if (sysfs_streq(config, "100")) {
        si5346_regs = si5346_revd_registers_100;
        strMHz = "100";
        numReg = SI5346_REVD_REG_CONFIG_NUM_REGS_100;
    } else if (sysfs_streq(config, "cus")) {
        si5346_regs = si5346_revd_registers_cus;
        strMHz = "88_100_100";
        numReg = SI5346_REVD_REG_CONFIG_NUM_REGS_CUS;
    } else if (sysfs_streq(config, "cus1")) {
        si5346_regs = si5346_revd_registers_cus1;
        strMHz = "88_150_FR";
        numReg = SI5346_REVD_REG_CONFIG_NUM_REGS_CUS1;
    } else if (sysfs_streq(config, "cus2")) {
        si5346_regs = si5346_revd_registers_cus2;
        strMHz = "88_150_150";
        numReg = SI5346_REVD_REG_CONFIG_NUM_REGS_CUS2;
    } else {
        /* strip newline from sysfs input string */
        char *newline;
        if ((newline=strchr(config, '\n')) != NULL)
            *newline = '\0';
        dev_info(&client->dev, "Unsupported output frequency %s MHz\n", config);
        /* Use previously set value */
        strMHz = si5346_data->cur_output;
        si5346_regs = si5346_data->regs;
    }
    dev_info(&client->dev, "Configuring output clock to %s MHz\n", strMHz);

    for (i=0; i<numReg; i++) {
        /*
           First 3 register writes require a 300 msec delay after completing
           to allow calibration to take affect
        */
        if (i == 3) {
            /* 300 msec delay after programming preamble */
            usleep_range(PREAMBLE_WAIT-10, PREAMBLE_WAIT+10);
        }
        /*  Register decoding:
         *
         *  Each register value is 16-bits:
         *   - 8 MSBs contain the page number
         *   - 8 LSBs contain the reg address
         *
         *  For example: 0x0813
         *   - Page 8, reg address 0x13
         */
        page = (unsigned char) (si5346_regs[i].address >> 8) & ADDR_MASK;
        addr = (unsigned char) si5346_regs[i].address & ADDR_MASK;
        data = si5346_regs[i].value;

        /* Only update page number when it changes.
           Page address is 0x01 on any page */
        if (page != curPage) {
            curPage = page;
            ret = i2c_smbus_write_byte_data(client, 0x01, curPage);
            if (ret) {
                dev_info(&client->dev, "Failed to write to page register (0x01)\r\n");
                return ret;
            }
        }
        /* Perform register byte write */
        ret = i2c_smbus_write_byte_data(client, addr, data);
        if (ret) {
            dev_info(&client->dev, "Failed to write to register (0%2x)\r\n", addr);
            return ret;
        }
    }
    dev_info(&client->dev, "Wrote %d configuration registers\r\n", i);
    /* Store currently set output frequency */
    si5346_data->cur_output = strMHz;
    si5346_data->regs = si5346_regs;

    return 0;
}

static ssize_t si5346_bin_read(struct file *filp, struct kobject *kobj,
    struct bin_attribute *attr, char *buf, loff_t off, size_t count)
{
    /* Exporting data to the user space is not supported, but this call can be
     * used for printing current status of the device */
    struct si5346_driver_data *si5346_data;

	si5346_data = dev_get_drvdata(container_of(kobj, struct device, kobj));

    dev_info(&si5346_data->client->dev,
             "Currently configured to output a %s MHz clock\n",
             si5346_data->cur_output);

    return 0;
}

static ssize_t si5346_bin_write(struct file *filp, struct kobject *kobj,
    struct bin_attribute *attr, char *buf, loff_t off, size_t count)
{
    struct si5346_driver_data *si5346_data;

	si5346_data = dev_get_drvdata(container_of(kobj, struct device, kobj));

    dev_info(&si5346_data->client->dev, "Count: %d\n", count);
    if (count > 5)
    {
        dev_err(&si5346_data->client->dev, "Input limited to 5 bytes \n");
    } else {
        //spin_lock(&si5346_data->status_mutex);
        si5346_init(si5346_data->client, (char*)buf);
        //spin_unlock(&si5346_data->status_mutex);
    }

	return count;
}

static int si5346_i2c_probe(struct i2c_client *client, const struct i2c_device_id *id)
{
    struct si5346_driver_data *si5346_data;
    int ret;

    dev_info(&client->dev, "Initialization\n");

    /* Allocate kernel memory for the device structure */
    si5346_data = devm_kzalloc(&client->dev, sizeof(struct si5346_driver_data), GFP_KERNEL);
    if (!si5346_data) {
        dev_info(&client->dev, "EEROR: Unable to allocate kernel memory for device\r\n");
        return -ENOMEM;
    }
    si5346_data->client = client;
    /* Initiaze the mutex */
    spin_lock_init(&si5346_data->status_mutex);

    /* Set device flag to master */
    i2c_set_clientdata(client, si5346_data);

    /* Sysfs api for accesing the device from the userspace */
    sysfs_bin_attr_init(&si5346_data->bin);
    si5346_data->bin.attr.name = DEVICE_NAME;
    si5346_data->bin.attr.mode = S_IRUSR | S_IWUSR;
    si5346_data->bin.write = si5346_bin_write;
    si5346_data->bin.read = si5346_bin_read;
    si5346_data->bin.size = 5;

    ret = sysfs_create_bin_file(&client->dev.kobj, &si5346_data->bin);
    if (ret)
        return ret;

    /* Get device version */
    ret = i2c_smbus_write_byte_data(client, 0x01, 0x0);
    if (ret) {
        dev_info(&client->dev, "Failed to set page register (0x01)\r\n");
    } else {
        dev_info(&client->dev, "Base part number is: %2x%2x\r\n",
                    i2c_smbus_read_byte_data(client, 0x03),
                    i2c_smbus_read_byte_data(client, 0x02));
    }

    /* Initialize output clock */
    ret = si5346_init(client, "88");
    if (ret) {
        dev_info(&client->dev, "Failed to initialize output clock\r\n");
    }
    return 0;
}

static int si5346_i2c_remove(struct i2c_client *client)
{
    struct si5346_driver_data *si5346_data = i2c_get_clientdata(client);

    sysfs_remove_bin_file(&client->dev.kobj, &si5346_data->bin);
    devm_kfree(&client->dev, si5346_data);
    dev_info(&client->dev, "Removed\r\n");

    return 0;
}

static const struct i2c_device_id si5346_i2c_id[] = {
    { DEVICE_NAME, 0 },
    { }
};
MODULE_DEVICE_TABLE(i2c, si5346_i2c_id);

static struct i2c_driver si5346_i2c_driver = {
    .driver = {
        .name = DRIVER_NAME,
    },
    .probe = si5346_i2c_probe,
    .remove = si5346_i2c_remove,
    .id_table = si5346_i2c_id,
};
module_i2c_driver(si5346_i2c_driver);

MODULE_DESCRIPTION("Interface for configuring Si5346 Digital PLL Clock Synthesizer");
MODULE_LICENSE("GPL v2");
